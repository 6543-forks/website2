# Website for XORM

This is official website of XORM which is based on doks theme of hugo

## How to run in dev mode?

First, you need to run

```
npm install
```

and then

```
npm run start
```

## How to release?
