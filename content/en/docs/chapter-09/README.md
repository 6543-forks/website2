---
title: "Execute SQL command"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-09"
weight: 2100
toc: true
---

## 9.Execute SQL command

When you want `insert`, `update` or `delete`, then use `Exec` as below

```Go
sql := "update userinfo set username=? where id=?"
res, err := engine.Exec(sql, "xiaolun", 1)
```
