---
title: "Prefix/Suffix/Cache name mapping"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-02"
weight: 9200
toc: true
---

### 2.2. Prefix mapping, Suffix Mapping and Cache Mapping

Decorated mappers can be created based on existing `names.Mapper` classes.

* `names.NewPrefixMapper(namesSnakeMapper{}, "prefix")` can add prefix string when naming based on SnakeMapper or SameMapper, or your custom Mapper.

For example,

```Go
tbMapper := names.NewPrefixMapper(names.SnakeMapper{}, "prefix_")
engine.SetTableMapper(tbMapper)
```

Then struct `type User struct` will be mapping to table `prefix_user` but not `user`.

* `names.NewSuffixMapper(names.SnakeMapper{}, "suffix")` can add suffix string when naming based on SnakeMapper or SameMapper, or your custom Mapper.
* `names.NewCacheMapper(names.SnakeMapper{})` can add a memory cache to an existing mapper.

Of course, you can implement IMapper to create a custom naming strategy.
