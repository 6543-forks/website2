---
title: "Execute SQL query"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-03"
weight: 3100
toc: true
---

## 8.Execute SQL query

Of course, SQL execution is also provided. If select then use Query

```Go
sql := "select * from userinfo"
results, err := engine.Query(sql)
```
